
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_21:20PM

See merge request itentialopensource/adapters/adapter-onap_dcae!16

---

## 0.6.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-onap_dcae!14

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:36PM

See merge request itentialopensource/adapters/adapter-onap_dcae!13

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_21:39PM

See merge request itentialopensource/adapters/adapter-onap_dcae!12

---

## 0.6.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!11

---

## 0.5.3 [03-27-2024]

* Changes made at 2024.03.27_13:22PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!10

---

## 0.5.2 [03-12-2024]

* Changes made at 2024.03.12_11:03AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!9

---

## 0.5.1 [02-27-2024]

* Changes made at 2024.02.27_11:39AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!8

---

## 0.5.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!7

---

## 0.4.0 [05-28-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!6

---

## 0.3.3 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!5

---

## 0.3.2 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!4

---

## 0.3.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!3

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!2

---

## 0.2.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-onap_dcae!1

---

## 0.1.1 [08-19-2019]

- Initial Commit

See commit 8464546

---
