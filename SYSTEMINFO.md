# ONAP Data Collection, Analytics and Events

Vendor: ONAP
Homepage: https://www.onap.org/

Product: Data Collection, Analytics and Events
Product Page: https://docs.onap.org/projects/onap-dcaegen2/en/latest/sections/architecture.html

## Introduction
We classify ONAP DCAE into the Service Assurance domain as ONAP DCAE devices provides a framework for collecting, analyzing, and processing data to support real-time, closed-loop control and automation in network management.

## Why Integrate
The ONAP DCAE adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP DCAE. With this adapter you have the ability to perform operations on items such as:

- Services

## Additional Product Documentation
The [API documents for ONAP DCAE](https://docs.onap.org/projects/onap-dcaegen2/en/latest/sections/offeredapis.html#offeredapis)
